#!/usr/bin/env node

const yargs = require('yargs')
const path = require('path')
const pkg = require('./package.json')
const postdoc = require('./')

const cli = yargs
  .usage('\nUsage: postdoc [options]')
  .describe('c', 'postdoc.config.js')
  .help('help')
  .alias('h', 'help')
  .version('v', pkg.version)
  .alias('v', 'version')

const resolve = dir => path.resolve(path.join('.', dir))

const options = {
  config: resolve(cli.argv.c || 'postdoc.config.js')
}

postdoc.render(options)
