const path = require('path')
const postdoc = require('../..')

const resolve = d => path.join(__dirname, d)

const config = {
  title: 'Basic Example',

  root: resolve('docs'),

  output: {
    path: resolve('build'),
    clean: true
  },

  plugins: [ postdoc.plugins.browsersync() ],

  theme: postdoc.themes.default(),

  sections: [ 'development', 'getting-started' ]
}

module.exports = config
