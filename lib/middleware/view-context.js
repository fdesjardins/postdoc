const { debug } = require('../utils/log')

/**
 * viewContext middleware
 */
const viewContext = async (ctx, next) => {
  debug('view context:', ctx.get())
  next()
}

module.exports = viewContext
