const { debug } = require('../utils/log')

/**
 * timer middleware
 */
const timer = stage => async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
  debug(`${stage} ${ctx.get('name')} - ${ms}ms`)
}

module.exports = timer
