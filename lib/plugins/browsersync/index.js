const { debug } = require('../../utils/log')

module.exports = options => {
  debug('browsersync plugin load', options)
  return {
    name: 'browsersync',
    watch: true
  }
}
