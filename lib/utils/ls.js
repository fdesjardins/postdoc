const path = require('path')
const { readdir, stat } = require('fs/promises')

const ls = async p =>
  (await readdir(p)).filter(async f =>
    (await stat(path.join(p, f))).isDirectory()
  )

module.exports = ls
