const chalk = require('chalk')
const safeStringify = require('fast-safe-stringify')

const levels = {
  info: chalk.blue('[info]'),
  warn: chalk.yellow('[warn]'),
  debug: chalk.green('[debug]'),
  error: chalk.red('[error]')
}

const stringifyDetails = details => {
  try {
    return JSON.stringify(details, null, 2)
  } catch (err) {
    return safeStringify(details, null, 2)
  }
}

const log = (level, msg, details) => {
  const timestamp = chalk.gray(`[${new Date().toISOString()}]`)
  const detailsString = stringifyDetails({ msg, details })

  console.log(`${level} ${timestamp} ${msg} ${detailsString}`)
}

module.exports = {
  info (msg, details) {
    log(levels.info, msg, details)
  },
  debug (msg, details) {
    log(levels.debug, msg, details)
  },
  warn (msg, details) {
    log(levels.warn, msg, details)
  },
  error (msg, details) {
    log(levels.error, msg, details)
  }
}
