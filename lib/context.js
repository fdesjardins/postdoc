const Baobab = require('baobab')
const joi = require('joi')
const { error, debug } = require('./utils/log')

const schema = joi.object().keys({
  config: joi.object().keys({
    title: joi.string().required()
  }),
  state: joi.string().required(),
  content: joi.string().required()
})

const validate = (previousState, newState, affectedPaths) => {
  debug('validate context', newState)
  const { err } = schema.validate(newState)
  if (err) {
    error(err)
  }
}

const context = new Baobab(
  {
    name: null,
    state: null,
    content: null
  },
  {
    validate
  }
)

module.exports = context
