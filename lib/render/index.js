const fs = require('fs')
const path = require('path')
const ls = require('../utils/ls')
const { debug, error } = require('../utils/log')

/**
 * getPlugins
 */
const getPlugins = config => {
  const plugins = []

  const browsersync = config.plugins.find(p => p.name === 'browsersync')
  if (browsersync) {
    debug('adding browsersync plugin')
    plugins.push(
      new BrowserSyncPlugin(
        {
          host: 'localhost',
          port: 34300,
          server: {
            baseDir: config.output.path
          },
          open: false
        },
        {
          reload: true
        }
      )
    )
  }

  return plugins
}

/**
 * compileIndex
 */
const compileIndex = async (config, webpackConfig) => {
  debug('compiling index', { config })

  const ctx = createContext(config)
  const readme = fs.readFileSync(path.join(config.root, 'readme.md'))
  ctx.set('content', readme.toString())

  const compiler = await createCompiler(ctx, config)
  await compiler(ctx)

  // TODO add error checking

  const output = ctx.get()

  const theme = config.theme

  const indexConfig = Object.assign({}, webpackConfig)
  indexConfig.plugins.push(
    new CleanWebpackPlugin([ config.output.path ], { allowExternal: true })
  )
  indexConfig.plugins.push(
    new CopyWebpackPlugin([
      {
        from: path.join(theme.root, 'favicon.ico'),
        to: 'favicon.ico'
      }
    ])
  )
  indexConfig.plugins.push(
    new HtmlWebpackPlugin({
      template: path.join(theme.root, 'index.html'),
      templateParameters: {
        title: config.title,
        content: output.content
      }
    })
  )
  indexConfig.plugins.push(new ExtractTextPlugin('styles.css'))

  webpack(indexConfig, (err, stats) => {
    debug('done rendering index')
    if (err) {
      error('error rendering index', err)
    }
  })
}

const compileSection = async (section, config, webpackConfig) => {
  debug('compiling section', section)

  const ctx = createContext(config)
  const sectionRoot = path.join(config.root, section)
  const files = await ls(sectionRoot)

  debug('files', files)
  files.map(async f => {
    const content = fs.readFileSync(path.join(sectionRoot, f))
    ctx.set('content', content.toString())
    const compiler = await createCompiler(ctx, config)
    await compiler(ctx)

    const output = ctx.get()
    // TODO add error checking
    const theme = config.theme

    // Create webpack config for each section file
    const fileConfig = Object.assign({}, webpackConfig)
    // fileConfig.entry = {}}
    fileConfig.output.path = path.join(
      config.output.path,
      section,
      f.split('.')[0]
    )
    fileConfig.plugins.push(new NoEmitPlugin())
    fileConfig.plugins.push(
      new HtmlWebpackPlugin({
        template: path.join(theme.root, 'index.html'),
        templateParameters: {
          title: `${config.title} - ${section}`,
          content: output.content
        }
      })
    )

    await new Promise((resolve, reject) => {
      webpack(fileConfig, (err, stats) => {
        debug('done rendering file', fileConfig)
        if (err) {
          error('error rendering section', err)
          return reject(err)
        }
        resolve()
      })
    })
  })
  debug('done rendering section')
}

/**
 * compileSections
 */
const compileSections = async (config, webpackConfig) => {
  debug('compileSections', { config, webpackConfig })

  const sections = config.sections || (await ls(config.root))

  debug('compiling sections', sections)
  // sections.map(section => compileSection(section, config, webpackConfig))
}

/**
 * createWebpackConfig
 */
const createWebpackConfig = config => {
  const theme = config.theme

  debug('creating index webpack config')
  const indexConfig = {
    mode: 'development',
    entry: {
      app: path.join(theme.root, 'lib/scripts')
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader'
          })
        }
      ]
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendors: {
            test: /node_modules/,
            name: 'common',
            chunks: 'all'
          }
        }
      }
    },
    output: {
      path: config.output.path
    },
    plugins: [ ...getPlugins(config) ],
    watch: !!config.plugins.find(p => p && p.watch === true)
  }

  debug('creating sections webpack config')
  const sectionsConfig = {
    mode: 'development',
    entry: {
      index: path.join(theme.root, 'index')
    },
    output: {
      path: config.output.path,
      filename: '[name].js'
    },
    plugins: [],
    watch: !!config.plugins.find(p => p && p.watch === true)
  }

  return {
    indexConfig,
    sectionsConfig
  }
}

/**
 * render
 */
const render = async options => {
  debug('start render', options)

  const config = require(options.config)
  debug('config:', config)

  // TODO add config validation

  if (!config) {
    error(
      'Invalid or missing postdoc configuration file. See postdoc documentation for setup instructions.',
      { config }
    )
    process.exit()
  }

  const { indexConfig, sectionsConfig } = createWebpackConfig(config)

  debug('compiling index')
  compileIndex(config, indexConfig)

  debug('compiling sections')
  compileSections(config, sectionsConfig)
}

module.exports = render
