const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const NoEmitPlugin = require('no-emit-webpack-plugin')
const compose = require('koa-compose')

/**
 * createCompiler
 */
const createCompiler = (ctx, config) => {
  const theme = config.theme
  const compiler = compose([
    async (ctx, next) => {
      debug('setting stage to "init"', ctx.get())
      ctx.set('stage', 'init')
      next()
    },
    ...theme.middleware.init,
    async (ctx, next) => {
      debug('setting stage to "compile"', ctx.get())
      ctx.set('stage', 'compile')
      next()
    },
    ...theme.middleware.compile,
    async (ctx, next) => {
      debug('setting stage to "copy"', ctx.get())
      ctx.set('stage', 'copy')
      next()
    }
  ])

  return compiler
}
