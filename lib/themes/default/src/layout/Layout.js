const TableOfContents = require('./TableOfContents')
const Article = require('./Article')

const Layout = ({ content }) => {
  return `
${TableOfContents()}
<main>
  ${Article}
</main>
`
}

module.exports = Layout
