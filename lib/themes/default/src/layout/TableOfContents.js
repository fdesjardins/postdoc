const TableOfContents = () => `
<nav class="toc ui vertical menu">
  <div class="item">
    <a class="ui logo icon image" href="/">
      <img src="/images/logo.png"/>
    </a>
    <a href="/"><b>postdoc</b></a>
  </div>

  <div class="ui category search item">
    <div class="ui transparent icon input">
      <input class="prompt" type="text" placeholder="Search content..."/>
      <i class="search link icon"></i>
    </div>
    <div class="results"></div>

    <a class="item" href="/introduction/getting-started.html">
      <b>Getting Started</b>
    </a>

    <div class="item">
      <div class="header">Intro</div>
      <div class="menu">
        <a class="item">Intro</a>
        <a class="item">Usage</a>
      </div>
    </div>

    <div class="item">
      <div class="header">Intro</div>
      <div class="menu">
        <a class="item">Intro</a>
        <a class="item">Usage</a>
      </div>
    </div>
  </div>
</nav>
`

module.exports = TableOfContents
