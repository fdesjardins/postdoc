const marked = require('marked')
const highlightJs = require('highlight.js')
const { timer, viewContext } = require('../../../middleware')
const { debug } = require('../../../utils/log')
const Layout = require('./layout/Layout')

/**
 * compile middleware
 */
const compile = async (ctx, next) => {
  debug('compile markdown')
  const content = ctx.get('content')
  marked.setOptions({
    highlight: code => highlightJs.highlightAuto(code).value,
    gfm: true,
    tables: true
  })
  ctx.set('content', marked(content))
  next()
}

/**
 * layout middleware
 */
const layout = async (ctx, next) => {
  debug('perform layout')
  const content = ctx.get('content')
  ctx.set('content', Layout({ content }))
  next()
}

/**
 * load default theme
 */
const load = options => {
  debug('default theme load', options)
  return {
    name: 'default-theme',
    root: __dirname,
    middleware: {
      init: [ timer('init'), viewContext ],
      compile: [ timer('compile'), compile, viewContext, layout ]
    }
  }
}

module.exports = load
