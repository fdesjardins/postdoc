module.exports = {
  plugins: require('./plugins'),
  themes: require('./themes'),
  render: require('./render'),
  context: require('./context')
}
