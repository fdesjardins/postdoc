# postdoc

[![Build Status][travis-image]][travis-url]
[![Coverage][coveralls-image]][coveralls-url]

Webpack-based automatic documentation generation for the modern age.

## Installation

## Usage

## Resources

## License

MIT © [Forrest Desjardins](https://github.com/fdesjardins)

[travis-url]: https://travis-ci.org/fdesjardins/postdoc
[travis-image]: https://img.shields.io/travis/fdesjardins/postdoc.svg?style=flat
[coveralls-url]: https://coveralls.io/r/fdesjardins/postdoc
[coveralls-image]: https://img.shields.io/coveralls/fdesjardins/postdoc.svg?style=flat
